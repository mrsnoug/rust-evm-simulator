use std::collections::HashMap;
use std::fmt::Write;
use std::time::{SystemTime, UNIX_EPOCH};

use primitive_types::U256;

use crate::evm_types::ExecutionStepOutcome;
use crate::evm_types::Opcode;
use crate::evm_types::RevertReason;

use ethsign::SecretKey;
use rand::{thread_rng, RngCore};

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Context {
    pub(crate) this_address: Vec<u8>,
    pub(crate) tx_origin: Vec<u8>,
    pub(crate) msg_sender: Vec<u8>,
    pub(crate) balances: HashMap<String, U256>, // String = hex representaion
    pub(crate) calldata: Vec<u8>,
    pub(crate) call_value: u128,
}

impl Default for Context {
    fn default() -> Self {
        Self::new()
    }
}

impl Context {
    pub fn new() -> Self {
        Self {
            this_address: Vec::with_capacity(20),
            tx_origin: Vec::with_capacity(20),
            msg_sender: Vec::with_capacity(20),
            balances: HashMap::new(),
            calldata: Vec::new(),
            call_value: 0,
        }
    }

    pub fn set_address(&mut self, address: Vec<u8>) {
        assert_eq!(address.len(), 20);
        self.this_address = address;
    }

    pub fn set_address_hex(&mut self, address: String) {
        let address_bytes = self.convert_hex_string_to_vec(address);
        assert_eq!(address_bytes.len(), 20);
        self.this_address = address_bytes;
    }

    pub fn set_tx_origin(&mut self, tx_origin: Vec<u8>) {
        assert_eq!(tx_origin.len(), 20);
        self.tx_origin = tx_origin;
    }

    pub fn set_tx_origin_hex(&mut self, tx_origin: String) {
        let tx_origin_bytes = self.convert_hex_string_to_vec(tx_origin);
        assert_eq!(tx_origin_bytes.len(), 20);
        self.tx_origin = tx_origin_bytes;
    }

    pub fn get_tx_origin(&self) -> Vec<u8> {
        self.tx_origin.clone()
    }

    pub fn get_tx_origin_hex(&self) -> String {
        self.convert_vec_to_hex_string(&self.tx_origin)
    }

    pub fn set_msg_sender(&mut self, msg_sender: Vec<u8>) {
        assert_eq!(msg_sender.len(), 20);
        self.msg_sender = msg_sender;
    }

    pub fn set_msg_sender_hex(&mut self, msg_sender: String) {
        let msg_sender_bytes = self.convert_hex_string_to_vec(msg_sender);
        assert_eq!(msg_sender_bytes.len(), 20);
        self.msg_sender = msg_sender_bytes;
    }

    pub fn get_msg_sender(&self) -> Vec<u8> {
        self.msg_sender.clone()
    }

    pub fn get_msg_sender_hex(&self) -> String {
        self.convert_vec_to_hex_string(&self.msg_sender)
    }

    pub fn get_address(&self) -> Vec<u8> {
        self.this_address.clone()
    }

    pub fn get_address_hex(&self) -> String {
        self.convert_vec_to_hex_string(&self.this_address)
    }

    pub fn get_balance_of(&self, address: U256) -> Option<&U256> {
        let address_hex = format!("{:x}", address);

        self.balances.get(&address_hex)
    }

    pub fn get_balance_of_hex(&self, address: String) -> Option<&U256> {
        self.balances.get(&address)
    }

    pub fn set_balance_of(&mut self, address: String, balance: U256) {
        self.balances.insert(address, balance);
    }

    pub fn set_call_value(&mut self, call_value: u128) {
        self.call_value = call_value;
    }

    pub fn get_call_value(&self) -> u128 {
        self.call_value
    }

    pub fn set_calldata(&mut self, calldata: Vec<u8>) {
        self.calldata = calldata;
    }

    pub fn get_calldata(&self) -> Vec<u8> {
        self.calldata.clone()
    }

    fn convert_hex_string_to_vec(&self, string: String) -> Vec<u8> {
        (0..string.len())
            .step_by(2)
            .map(|i| u8::from_str_radix(&string[i..i + 2], 16).unwrap())
            .collect()
    }

    fn convert_vec_to_hex_string(&self, bytes: &Vec<u8>) -> String {
        let mut s = String::with_capacity(bytes.len() * 2);

        for byte in bytes {
            write!(&mut s, "{:02x}", byte).unwrap();
        }
        s
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum EvmExecutionError {
    Stopped,
    Reverted,
    GasExceeded,
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Evm {
    pub(crate) stack: Vec<U256>,

    // In EVM it's actually a ROM, but for simplicity I decided to use Vec
    pub(crate) code: Vec<Opcode>,
    pub(crate) code_bytes: Vec<u8>,

    // program counter
    pub(crate) pc: usize,
    pub(crate) memory: Vec<u8>,
    pub(crate) last_accessed_memory_offset: usize,
    pub(crate) last_memory_cost: usize,
    pub(crate) storage: HashMap<U256, U256>,
    pub(crate) gas: u128,
    pub(crate) context: Context,
    pub(crate) gas_price: u128,
    pub(crate) other_contracts_code: HashMap<String, Vec<u8>>, // dirty hack, but it's just for fun
    pub(crate) current_block_number: u128,
    pub(crate) block_number_to_hash: HashMap<u128, String>,
    pub(crate) current_block_miner: Option<String>,
    pub(crate) timestamp: Option<u128>,
    pub(crate) block_difficulty: u128,
    pub(crate) gas_limit: u128,
    pub(crate) chain_id: u128,
}

impl Default for Evm {
    fn default() -> Self {
        Self::new()
    }
}

impl Evm {
    pub fn new() -> Self {
        Self {
            stack: Vec::new(),
            code: Vec::new(),
            code_bytes: Vec::new(),
            pc: 0,
            memory: Vec::new(),
            last_accessed_memory_offset: 0,
            last_memory_cost: 0,
            storage: HashMap::default(),
            gas: 0,
            context: Context::new(),
            gas_price: 10,
            other_contracts_code: HashMap::new(),
            current_block_number: 1,
            block_number_to_hash: HashMap::new(),
            current_block_miner: None,
            timestamp: Some(
                SystemTime::now()
                    .duration_since(UNIX_EPOCH)
                    .unwrap()
                    .as_millis(),
            ),
            block_difficulty: 10895004000000000,
            gas_limit: 0xffffffffffff,
            chain_id: 123123123,
        }
    }

    pub fn get_code(&self) -> Vec<Opcode> {
        self.code.clone()
    }

    pub fn get_stack(&self) -> Vec<U256> {
        self.stack.clone()
    }

    pub fn set_stack(&mut self, stack: Vec<U256>) {
        self.stack = stack;
    }

    pub fn get_gas(&self) -> u128 {
        self.gas
    }

    pub fn set_gas(&mut self, gas: u128) {
        self.gas = gas;
    }

    pub fn add_opcode(&mut self, opcode: Opcode) {
        self.code.push(opcode);
    }

    pub fn set_context(&mut self, context: Context) {
        self.context = context;
    }

    pub fn set_memory(&mut self, memory: Vec<u8>) {
        self.memory = memory;
    }

    pub fn get_memory_part(&self, offset: usize, size: usize) -> &[u8] {
        &self.memory[offset..size]
    }

    pub(crate) fn allocate_more_memory(&mut self, offset: usize, size: usize) {
        if self.memory.len() < offset {
            for _ in 0..(offset - self.memory.len()) {
                self.memory.push(0);
            }
        }

        if self.memory.len() < offset + size {
            for _ in 0..((offset + size) - self.memory.len()) {
                self.memory.push(0);
            }
        }
    }

    pub fn set_storage(&mut self, storage: HashMap<U256, U256>) {
        self.storage = storage;
    }

    pub fn set_current_block_number(&mut self, block_number: u128) {
        self.current_block_number = block_number;
    }

    pub fn get_block_number(&self) -> u128 {
        self.current_block_number
    }

    pub fn set_block_hash_of(&mut self, block_number: u128, hash: String) {
        self.block_number_to_hash.insert(block_number, hash);
    }

    pub fn get_block_hash_of(&self, block_number: u128) -> Option<&String> {
        self.block_number_to_hash.get(&block_number)
    }

    pub fn get_current_block_miner(&self) -> String {
        if let Some(miner) = &self.current_block_miner {
            miner.clone()
        } else {
            // generating an eth address
            let mut random = [0u8, 32];
            thread_rng().fill_bytes(&mut random);
            let secret = SecretKey::from_raw(&random).unwrap();
            let pubkey = secret.public();
            let address = pubkey.address();

            let mut address_str = String::with_capacity(40);
            for byte in address {
                write!(&mut address_str, "{:02x}", byte).unwrap();
            }
            address_str
        }
    }

    pub fn get_timestamp(&self) -> u128 {
        if let Some(ts) = self.timestamp {
            ts
        } else {
            SystemTime::now()
                .duration_since(UNIX_EPOCH)
                .unwrap()
                .as_millis()
        }
    }

    pub fn set_timestamp(&mut self, new_timestamp: u128) {
        self.timestamp = Some(new_timestamp);
    }

    pub fn set_timestamp_now(&mut self) {
        self.timestamp = Some(
            SystemTime::now()
                .duration_since(UNIX_EPOCH)
                .unwrap()
                .as_millis(),
        );
    }

    pub fn get_current_difficulty(&self) -> u128 {
        self.block_difficulty
    }

    pub fn set_current_difficulty(&mut self, new_difficulty: u128) {
        self.block_difficulty = new_difficulty;
    }

    pub fn get_gas_limit(&self) -> u128 {
        self.gas_limit
    }

    pub fn set_gas_limit(&mut self, gas_limit: u128) {
        self.gas_limit = gas_limit;
    }

    pub fn get_chain_id(&self) -> u128 {
        self.chain_id
    }

    pub fn set_chain_id(&mut self, chain_id: u128) {
        self.chain_id = chain_id;
    }

    pub fn fast_forward(&mut self, _number_of_blocks: u128) {
        // Fast forward EVM by number_of_blocks. Chagne the current_block_number,
        // create dummy hashes for intermediate blocks, etc.
        todo!();
    }

    pub fn parse_bytecode(bytes: String) -> Self {
        let mut this = Self::new();

        let code = Opcode::opcodes_from_bytecode(bytes.clone());
        this.code = code;

        let code_bytes: Vec<u8> = (0..bytes.len())
            .step_by(2)
            .map(|i| u8::from_str_radix(&bytes[i..i + 2], 16).unwrap())
            .collect();
        this.code_bytes = code_bytes;

        this
    }

    pub fn execute(&mut self) -> Result<Option<U256>, EvmExecutionError> {
        assert!(self.is_code_valid());

        let storage_snapshot = self.storage.clone();

        while self.pc < self.code.len() {
            let outcome = self.execute_step();

            if let ExecutionStepOutcome::Revert(reason) = outcome {
                match reason {
                    RevertReason::GasExceeded => {
                        self.storage = storage_snapshot;
                        return Err(EvmExecutionError::GasExceeded);
                    }
                    RevertReason::NotEnoughValuesOnStack => {
                        self.storage = storage_snapshot;
                        return Err(EvmExecutionError::Reverted);
                    }
                    RevertReason::Unknown => panic!("Unknown error"),
                }
            }

            if outcome == ExecutionStepOutcome::Stop {
                return Err(EvmExecutionError::Stopped);
            }

            if let ExecutionStepOutcome::Return(_return_val) = outcome {
                break;
            }
        }
        todo!();
    }

    pub fn execute_step(&mut self) -> ExecutionStepOutcome {
        let this_opcode = self.code[self.pc].clone();

        let step_outcome = Opcode::execute_opcode(this_opcode, self);

        self.pc += 1;

        step_outcome
    }

    pub fn is_code_valid(&self) -> bool {
        for opcode in &self.code {
            if *opcode == Opcode::INVALID {
                return false;
            }
        }

        true
    }
}
