use hex::FromHex;
use primitive_types::{U256, U512};
use std::str::FromStr;

use super::{Evm, Sign, I256};
use sha3::{Digest, Keccak256};

// const SIGN_BIT_MASK: U256 = U256([
//     0xffffffffffffffff,
//     0xffffffffffffffff,
//     0xffffffffffffffff,
//     0x7fffffffffffffff,
// ]);

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum ExecutionStepOutcome {
    Ok,
    Revert(RevertReason),
    Return(Option<U256>),
    Stop,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum RevertReason {
    GasExceeded,
    NotEnoughValuesOnStack,
    Unknown,
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub enum Opcode {
    STOP,
    ADD,
    MUL,
    SUB,
    DIV,
    SDIV,
    MOD,
    SMOD,
    ADDMOD,
    MULMOD,
    EXP,
    SIGNEXTEND,
    LT,
    GT,
    SLT,
    SGT,
    EQ,
    ISZERO,
    AND,
    OR,
    XOR,
    NOT,
    BYTE,
    SHL,
    SHR,
    SAR,
    SHA3,
    ADDRESS,
    BALANCE,
    ORIGIN,
    CALLER,
    CALLVALUE,
    CALLDATALOAD,
    CALLDATASIZE,
    CALLDATACOPY,
    CODESIZE,
    CODECOPY,
    GASPRICE,
    EXTCODESIZE,
    EXTCODECOPY,
    RETURNDATASIZE,
    RETURNDATACOPY,
    EXTCODEHASH,
    BLOCKHASH,
    COINBASE,
    TIMESTAMP,
    NUMBER,
    DIFFICULTY,
    GASLIMIT,
    CHAINID,
    SELFBALANCE,
    BASEFEE,
    POP,
    MLOAD,
    MSTORE,
    MSTORE8,
    SLOAD,
    SSTORE,
    JUMP,
    JUMPI,
    PC,
    MSIZE,
    GAS,
    JUMPDEST,
    PUSH1(Vec<u8>),
    PUSH2(Vec<u8>),
    PUSH3(Vec<u8>),
    PUSH4(Vec<u8>),
    PUSH5(Vec<u8>),
    PUSH6(Vec<u8>),
    PUSH7(Vec<u8>),
    PUSH8(Vec<u8>),
    PUSH9(Vec<u8>),
    PUSH10(Vec<u8>),
    PUSH11(Vec<u8>),
    PUSH12(Vec<u8>),
    PUSH13(Vec<u8>),
    PUSH14(Vec<u8>),
    PUSH15(Vec<u8>),
    PUSH16(Vec<u8>),
    PUSH17(Vec<u8>),
    PUSH18(Vec<u8>),
    PUSH19(Vec<u8>),
    PUSH20(Vec<u8>),
    PUSH21(Vec<u8>),
    PUSH22(Vec<u8>),
    PUSH23(Vec<u8>),
    PUSH24(Vec<u8>),
    PUSH25(Vec<u8>),
    PUSH26(Vec<u8>),
    PUSH27(Vec<u8>),
    PUSH28(Vec<u8>),
    PUSH29(Vec<u8>),
    PUSH30(Vec<u8>),
    PUSH31(Vec<u8>),
    PUSH32(Vec<u8>),
    DUP1,
    DUP2,
    DUP3,
    DUP4,
    DUP5,
    DUP6,
    DUP7,
    DUP8,
    DUP9,
    DUP10,
    DUP11,
    DUP12,
    DUP13,
    DUP14,
    DUP15,
    DUP16,
    SWAP1,
    SWAP2,
    SWAP3,
    SWAP4,
    SWAP5,
    SWAP6,
    SWAP7,
    SWAP8,
    SWAP9,
    SWAP10,
    SWAP11,
    SWAP12,
    SWAP13,
    SWAP14,
    SWAP15,
    SWAP16,
    LOG0,
    LOG1,
    LOG2,
    LOG3,
    LOG4,
    CREATE,
    CALL,
    CALLCODE,
    RETURN,
    DELEGATECALL,
    CREATE2,
    STATICCALL,
    REVERT,
    SELFDESTRUCT,
    INVALID,
}

impl Opcode {
    pub fn opcodes_from_bytecode(bytes_string: String) -> Vec<Opcode> {
        let mut code = Vec::new();

        let bytecode = Vec::from_hex(bytes_string).unwrap();
        let all_bytes = bytecode.len();
        let mut bytes_processed = 0;
        let mut bytecode_iter = bytecode.iter();

        while bytes_processed < all_bytes {
            let current_opcode_byte = bytecode_iter.next().unwrap();

            let opcode = match current_opcode_byte {
                0x00 => Opcode::STOP,
                0x01 => Opcode::ADD,
                0x02 => Opcode::MUL,
                0x03 => Opcode::SUB,
                0x04 => Opcode::DIV,
                0x05 => Opcode::SDIV,
                0x06 => Opcode::MOD,
                0x07 => Opcode::SMOD,
                0x08 => Opcode::ADDMOD,
                0x09 => Opcode::MULMOD,
                0x0A => Opcode::EXP,
                0x0B => Opcode::SIGNEXTEND,
                0x10 => Opcode::LT,
                0x11 => Opcode::GT,
                0x12 => Opcode::SLT,
                0x13 => Opcode::SGT,
                0x14 => Opcode::EQ,
                0x15 => Opcode::ISZERO,
                0x16 => Opcode::AND,
                0x17 => Opcode::OR,
                0x18 => Opcode::XOR,
                0x19 => Opcode::NOT,
                0x1A => Opcode::BYTE,
                0x1B => Opcode::SHL,
                0x1C => Opcode::SHR,
                0x1D => Opcode::SAR,
                0x20 => Opcode::SHA3,
                0x30 => Opcode::ADDRESS,
                0x31 => Opcode::BALANCE,
                0x32 => Opcode::ORIGIN,
                0x33 => Opcode::CALLER,
                0x34 => Opcode::CALLVALUE,
                0x35 => Opcode::CALLDATALOAD,
                0x36 => Opcode::CALLDATASIZE,
                0x37 => Opcode::CALLDATACOPY,
                0x38 => Opcode::CODECOPY,
                0x3A => Opcode::GASPRICE,
                0x3B => Opcode::EXTCODESIZE,
                0x3C => Opcode::EXTCODECOPY,
                0x3D => Opcode::RETURNDATASIZE,
                0x3E => Opcode::RETURNDATACOPY,
                0x3F => Opcode::EXTCODEHASH,
                0x40 => Opcode::BLOCKHASH,
                0x41 => Opcode::COINBASE,
                0x42 => Opcode::TIMESTAMP,
                0x43 => Opcode::NUMBER,
                0x44 => Opcode::DIFFICULTY,
                0x45 => Opcode::GASLIMIT,
                0x46 => Opcode::CHAINID,
                0x47 => Opcode::SELFBALANCE,
                0x48 => Opcode::BASEFEE,
                0x50 => Opcode::POP,
                0x51 => Opcode::MLOAD,
                0x52 => Opcode::MSTORE,
                0x53 => Opcode::MSTORE8,
                0x54 => Opcode::SLOAD,
                0x55 => Opcode::SSTORE,
                0x56 => Opcode::JUMP,
                0x57 => Opcode::JUMPI,
                0x58 => Opcode::PC,
                0x59 => Opcode::MSIZE,
                0x5A => Opcode::GAS,
                0x5B => Opcode::JUMPDEST,
                0x60 => {
                    let value = bytecode_iter.next().unwrap();
                    bytes_processed += 1;
                    Opcode::PUSH1(vec![*value])
                }
                0x61 => {
                    let mut vals = Vec::with_capacity(2);
                    for _ in 0..2 {
                        vals.push(*bytecode_iter.next().unwrap());
                    }
                    bytes_processed += 2;
                    Opcode::PUSH2(vals)
                }
                0x62 => {
                    let mut vals = Vec::with_capacity(3);
                    for _ in 0..3 {
                        vals.push(*bytecode_iter.next().unwrap());
                    }

                    bytes_processed += 3;
                    Opcode::PUSH3(vals)
                }
                0x63 => {
                    let mut vals = Vec::with_capacity(4);
                    for _ in 0..4 {
                        vals.push(*bytecode_iter.next().unwrap());
                    }

                    bytes_processed += 4;
                    Opcode::PUSH4(vals)
                }
                0x64 => {
                    let mut vals = Vec::with_capacity(5);
                    for _ in 0..5 {
                        vals.push(*bytecode_iter.next().unwrap());
                    }

                    bytes_processed += 5;
                    Opcode::PUSH5(vals)
                }
                0x65 => {
                    let mut vals = Vec::with_capacity(6);
                    for _ in 0..6 {
                        vals.push(*bytecode_iter.next().unwrap());
                    }

                    bytes_processed += 6;
                    Opcode::PUSH6(vals)
                }
                0x66 => {
                    let mut vals = Vec::with_capacity(7);
                    for _ in 0..7 {
                        vals.push(*bytecode_iter.next().unwrap());
                    }

                    bytes_processed += 7;
                    Opcode::PUSH7(vals)
                }
                0x67 => {
                    let mut vals = Vec::with_capacity(8);
                    for _ in 0..8 {
                        vals.push(*bytecode_iter.next().unwrap());
                    }

                    bytes_processed += 8;
                    Opcode::PUSH8(vals)
                }
                0x68 => {
                    let mut vals = Vec::with_capacity(9);
                    for _ in 0..9 {
                        vals.push(*bytecode_iter.next().unwrap());
                    }

                    bytes_processed += 9;
                    Opcode::PUSH9(vals)
                }
                0x69 => {
                    let mut vals = Vec::with_capacity(10);
                    for _ in 0..10 {
                        vals.push(*bytecode_iter.next().unwrap());
                    }

                    bytes_processed += 10;
                    Opcode::PUSH10(vals)
                }
                0x6A => {
                    let mut vals = Vec::with_capacity(11);
                    for _ in 0..11 {
                        vals.push(*bytecode_iter.next().unwrap());
                    }

                    bytes_processed += 11;
                    Opcode::PUSH11(vals)
                }
                0x6B => {
                    let mut vals = Vec::with_capacity(12);
                    for _ in 0..12 {
                        vals.push(*bytecode_iter.next().unwrap());
                    }

                    bytes_processed += 12;
                    Opcode::PUSH10(vals)
                }
                0x6C => {
                    let mut vals = Vec::with_capacity(13);
                    for _ in 0..13 {
                        vals.push(*bytecode_iter.next().unwrap());
                    }

                    bytes_processed += 13;
                    Opcode::PUSH13(vals)
                }
                0x6D => {
                    let mut vals = Vec::with_capacity(14);
                    for _ in 0..14 {
                        vals.push(*bytecode_iter.next().unwrap());
                    }

                    bytes_processed += 14;
                    Opcode::PUSH14(vals)
                }
                0x6E => {
                    let mut vals = Vec::with_capacity(15);
                    for _ in 0..15 {
                        vals.push(*bytecode_iter.next().unwrap());
                    }

                    bytes_processed += 15;
                    Opcode::PUSH15(vals)
                }
                0x6F => {
                    let mut vals = Vec::with_capacity(16);
                    for _ in 0..16 {
                        vals.push(*bytecode_iter.next().unwrap());
                    }

                    bytes_processed += 16;
                    Opcode::PUSH16(vals)
                }
                0x70 => {
                    let mut vals = Vec::with_capacity(17);
                    for _ in 0..17 {
                        vals.push(*bytecode_iter.next().unwrap());
                    }

                    bytes_processed += 17;
                    Opcode::PUSH17(vals)
                }
                0x71 => {
                    let mut vals = Vec::with_capacity(18);
                    for _ in 0..18 {
                        vals.push(*bytecode_iter.next().unwrap());
                    }

                    bytes_processed += 18;
                    Opcode::PUSH18(vals)
                }
                0x72 => {
                    let mut vals = Vec::with_capacity(19);
                    for _ in 0..19 {
                        vals.push(*bytecode_iter.next().unwrap());
                    }

                    bytes_processed += 19;
                    Opcode::PUSH19(vals)
                }
                0x73 => {
                    let mut vals = Vec::with_capacity(20);
                    for _ in 0..20 {
                        vals.push(*bytecode_iter.next().unwrap());
                    }

                    bytes_processed += 20;
                    Opcode::PUSH20(vals)
                }
                0x74 => {
                    let mut vals = Vec::with_capacity(21);
                    for _ in 0..21 {
                        vals.push(*bytecode_iter.next().unwrap());
                    }

                    bytes_processed += 21;
                    Opcode::PUSH21(vals)
                }
                0x75 => {
                    let mut vals = Vec::with_capacity(22);
                    for _ in 0..22 {
                        vals.push(*bytecode_iter.next().unwrap());
                    }

                    bytes_processed += 22;
                    Opcode::PUSH22(vals)
                }
                0x76 => {
                    let mut vals = Vec::with_capacity(23);
                    for _ in 0..23 {
                        vals.push(*bytecode_iter.next().unwrap());
                    }

                    bytes_processed += 23;
                    Opcode::PUSH23(vals)
                }
                0x77 => {
                    let mut vals = Vec::with_capacity(24);
                    for _ in 0..24 {
                        vals.push(*bytecode_iter.next().unwrap());
                    }

                    bytes_processed += 24;
                    Opcode::PUSH24(vals)
                }
                0x78 => {
                    let mut vals = Vec::with_capacity(25);
                    for _ in 0..25 {
                        vals.push(*bytecode_iter.next().unwrap());
                    }

                    bytes_processed += 25;
                    Opcode::PUSH25(vals)
                }
                0x79 => {
                    let mut vals = Vec::with_capacity(26);
                    for _ in 0..26 {
                        vals.push(*bytecode_iter.next().unwrap());
                    }

                    bytes_processed += 26;
                    Opcode::PUSH26(vals)
                }
                0x7A => {
                    let mut vals = Vec::with_capacity(27);
                    for _ in 0..27 {
                        vals.push(*bytecode_iter.next().unwrap());
                    }

                    bytes_processed += 27;
                    Opcode::PUSH27(vals)
                }
                0x7B => {
                    let mut vals = Vec::with_capacity(28);
                    for _ in 0..28 {
                        vals.push(*bytecode_iter.next().unwrap());
                    }

                    bytes_processed += 28;
                    Opcode::PUSH28(vals)
                }
                0x7C => {
                    let mut vals = Vec::with_capacity(29);
                    for _ in 0..29 {
                        vals.push(*bytecode_iter.next().unwrap());
                    }

                    bytes_processed += 29;
                    Opcode::PUSH29(vals)
                }
                0x7D => {
                    let mut vals = Vec::with_capacity(30);
                    for _ in 0..30 {
                        vals.push(*bytecode_iter.next().unwrap());
                    }

                    bytes_processed += 30;
                    Opcode::PUSH30(vals)
                }
                0x7E => {
                    let mut vals = Vec::with_capacity(31);
                    for _ in 0..31 {
                        vals.push(*bytecode_iter.next().unwrap());
                    }

                    bytes_processed += 31;
                    Opcode::PUSH31(vals)
                }
                0x7F => {
                    let mut vals = Vec::with_capacity(32);
                    for _ in 0..32 {
                        vals.push(*bytecode_iter.next().unwrap());
                    }

                    bytes_processed += 32;
                    Opcode::PUSH32(vals)
                }
                0x80 => Opcode::DUP1,
                0x81 => Opcode::DUP2,
                0x82 => Opcode::DUP3,
                0x83 => Opcode::DUP4,
                0x84 => Opcode::DUP5,
                0x85 => Opcode::DUP6,
                0x86 => Opcode::DUP7,
                0x87 => Opcode::DUP8,
                0x88 => Opcode::DUP9,
                0x89 => Opcode::DUP10,
                0x8A => Opcode::DUP11,
                0x8B => Opcode::DUP12,
                0x8C => Opcode::DUP13,
                0x8D => Opcode::DUP14,
                0x8E => Opcode::DUP15,
                0x8F => Opcode::DUP16,
                0x90 => Opcode::SWAP1,
                0x91 => Opcode::SWAP2,
                0x92 => Opcode::SWAP3,
                0x93 => Opcode::SWAP4,
                0x94 => Opcode::SWAP5,
                0x95 => Opcode::SWAP6,
                0x96 => Opcode::SWAP7,
                0x97 => Opcode::SWAP8,
                0x98 => Opcode::SWAP9,
                0x99 => Opcode::SWAP10,
                0x9A => Opcode::SWAP11,
                0x9B => Opcode::SWAP12,
                0x9C => Opcode::SWAP13,
                0x9D => Opcode::SWAP14,
                0x9E => Opcode::SWAP15,
                0x9F => Opcode::SWAP16,
                0xA0 => Opcode::LOG0,
                0xA1 => Opcode::LOG1,
                0xA2 => Opcode::LOG2,
                0xA3 => Opcode::LOG3,
                0xA4 => Opcode::LOG4,
                0xF0 => Opcode::CREATE,
                0xF1 => Opcode::CALL,
                0xF2 => Opcode::CALLCODE,
                0xF3 => Opcode::RETURN,
                0xF4 => Opcode::DELEGATECALL,
                0xF5 => Opcode::CREATE2,
                0xFA => Opcode::STATICCALL,
                0xFD => Opcode::REVERT,
                0xFF => Opcode::SELFDESTRUCT,
                _ => Opcode::INVALID,
            };

            code.push(opcode);
            // processed opcode
            bytes_processed += 1;
        }

        code
    }

    fn exceeded_prepaid_gas(evm: &mut Evm, opcode_gas_cost: u128) -> bool {
        if evm.gas < opcode_gas_cost {
            true
        } else {
            evm.gas -= opcode_gas_cost;
            false
        }
    }

    pub fn execute_opcode(opcode: Opcode, evm: &mut Evm) -> ExecutionStepOutcome {
        match opcode {
            Opcode::STOP => ExecutionStepOutcome::Stop,
            Opcode::ADD => {
                if Opcode::exceeded_prepaid_gas(evm, 3) {
                    return ExecutionStepOutcome::Revert(RevertReason::GasExceeded);
                }
                let a = evm.stack.pop();
                let b = evm.stack.pop();
                if a.is_none() || b.is_none() {
                    return ExecutionStepOutcome::Revert(RevertReason::NotEnoughValuesOnStack);
                }
                evm.stack.push(a.unwrap() + b.unwrap());

                ExecutionStepOutcome::Ok
            }
            Opcode::MUL => {
                if Opcode::exceeded_prepaid_gas(evm, 5) {
                    return ExecutionStepOutcome::Revert(RevertReason::GasExceeded);
                }
                let a = evm.stack.pop();
                let b = evm.stack.pop();
                if a.is_none() || b.is_none() {
                    return ExecutionStepOutcome::Revert(RevertReason::NotEnoughValuesOnStack);
                }
                evm.stack.push(a.unwrap().overflowing_mul(b.unwrap()).0);
                ExecutionStepOutcome::Ok
            }
            Opcode::SUB => {
                if Opcode::exceeded_prepaid_gas(evm, 3) {
                    return ExecutionStepOutcome::Revert(RevertReason::GasExceeded);
                }
                let a = evm.stack.pop();
                let b = evm.stack.pop();
                if a.is_none() || b.is_none() {
                    return ExecutionStepOutcome::Revert(RevertReason::NotEnoughValuesOnStack);
                }
                evm.stack.push(a.unwrap().overflowing_sub(b.unwrap()).0);
                ExecutionStepOutcome::Ok
            }
            Opcode::DIV => {
                if Opcode::exceeded_prepaid_gas(evm, 5) {
                    return ExecutionStepOutcome::Revert(RevertReason::GasExceeded);
                }
                let a = evm.stack.pop();
                let b = evm.stack.pop();
                if a.is_none() || b.is_none() {
                    return ExecutionStepOutcome::Revert(RevertReason::NotEnoughValuesOnStack);
                }
                let b = b.unwrap();
                if b == U256::from(0) {
                    evm.stack.push(U256::from(0));
                    return ExecutionStepOutcome::Ok;
                }
                evm.stack.push(a.unwrap() / b);
                ExecutionStepOutcome::Ok
            }
            Opcode::SDIV => {
                if Opcode::exceeded_prepaid_gas(evm, 5) {
                    return ExecutionStepOutcome::Revert(RevertReason::GasExceeded);
                }
                let a = evm.stack.pop();
                let b = evm.stack.pop();
                if a.is_none() || b.is_none() {
                    return ExecutionStepOutcome::Revert(RevertReason::NotEnoughValuesOnStack);
                }
                let b = b.unwrap();
                if b == U256::from(0) {
                    evm.stack.push(U256::from(0));
                    return ExecutionStepOutcome::Ok;
                }

                let a_signed = I256::from(a.unwrap());
                let b_signed = I256::from(b);

                let result = a_signed / b_signed;

                evm.stack.push(U256::from(result));
                ExecutionStepOutcome::Ok
            }
            Opcode::MOD => {
                if Opcode::exceeded_prepaid_gas(evm, 5) {
                    return ExecutionStepOutcome::Revert(RevertReason::GasExceeded);
                }
                let a = evm.stack.pop();
                let b = evm.stack.pop();
                if a.is_none() || b.is_none() {
                    return ExecutionStepOutcome::Revert(RevertReason::NotEnoughValuesOnStack);
                }
                let b = b.unwrap();
                if b == U256::from(0) {
                    evm.stack.push(U256::from(0));
                    return ExecutionStepOutcome::Ok;
                }

                evm.stack.push(a.unwrap() % b);
                ExecutionStepOutcome::Ok
            }
            Opcode::SMOD => {
                if Opcode::exceeded_prepaid_gas(evm, 5) {
                    return ExecutionStepOutcome::Revert(RevertReason::GasExceeded);
                }
                let a = evm.stack.pop();
                let b = evm.stack.pop();
                if a.is_none() || b.is_none() {
                    return ExecutionStepOutcome::Revert(RevertReason::NotEnoughValuesOnStack);
                }
                let b = b.unwrap();
                if b == U256::from(0) {
                    evm.stack.push(U256::from(0));
                    return ExecutionStepOutcome::Ok;
                }

                let a_signed = I256::from(a.unwrap());
                let b_signed = I256::from(b);

                let result = a_signed % b_signed;

                evm.stack.push(U256::from(result));
                ExecutionStepOutcome::Ok
            }
            Opcode::ADDMOD => {
                if Opcode::exceeded_prepaid_gas(evm, 8) {
                    return ExecutionStepOutcome::Revert(RevertReason::GasExceeded);
                }
                let a = evm.stack.pop();
                let b = evm.stack.pop();
                let n = evm.stack.pop();

                if a.is_none() || b.is_none() || n.is_none() {
                    return ExecutionStepOutcome::Revert(RevertReason::NotEnoughValuesOnStack);
                }

                let n_signed = I256::from(n.unwrap());

                let result = I256::from(a.unwrap().overflowing_add(b.unwrap()).0) % n_signed;
                evm.stack.push(U256::from(result));

                ExecutionStepOutcome::Ok
            }
            Opcode::MULMOD => {
                if Opcode::exceeded_prepaid_gas(evm, 8) {
                    return ExecutionStepOutcome::Revert(RevertReason::GasExceeded);
                }

                let a = evm.stack.pop();
                let b = evm.stack.pop();
                let n = evm.stack.pop();

                if a.is_none() || b.is_none() || n.is_none() {
                    return ExecutionStepOutcome::Revert(RevertReason::NotEnoughValuesOnStack);
                }

                if n.unwrap() == U256::from(0) {
                    evm.stack.push(U256::from(0));
                    return ExecutionStepOutcome::Ok;
                }

                let n_signed = I256::from(n.unwrap());
                let mul_res = U512::from(a.unwrap()) * U512::from(b.unwrap());
                let result = mul_res % n_signed.1;
                let res_u256: U256 = result.try_into().unwrap();
                evm.stack.push(res_u256);

                ExecutionStepOutcome::Ok
            }
            Opcode::EXP => {
                let a = evm.stack.pop();
                let exponent = evm.stack.pop();

                if a.is_none() || exponent.is_none() {
                    return ExecutionStepOutcome::Revert(RevertReason::NotEnoughValuesOnStack);
                }

                let a = a.unwrap();
                let exponent = exponent.unwrap();

                // 10 static + 50 * exponent_byte_size
                let gas = (10 + 50 * (exponent.bits() / 8 + 1)) as u128;
                if Opcode::exceeded_prepaid_gas(evm, gas) {
                    return ExecutionStepOutcome::Revert(RevertReason::GasExceeded);
                }

                let result = a.overflowing_pow(exponent);
                evm.stack.push(result.0);

                ExecutionStepOutcome::Ok
            }
            Opcode::SIGNEXTEND => todo!(),
            Opcode::LT => {
                if Opcode::exceeded_prepaid_gas(evm, 3) {
                    return ExecutionStepOutcome::Revert(RevertReason::GasExceeded);
                }
                let a = evm.stack.pop();
                let b = evm.stack.pop();

                if a.is_none() || b.is_none() {
                    return ExecutionStepOutcome::Revert(RevertReason::NotEnoughValuesOnStack);
                }

                if a.unwrap() < b.unwrap() {
                    evm.stack.push(U256::from(1));
                } else {
                    evm.stack.push(U256::from(0));
                }
                ExecutionStepOutcome::Ok
            }
            Opcode::GT => {
                if Opcode::exceeded_prepaid_gas(evm, 3) {
                    return ExecutionStepOutcome::Revert(RevertReason::GasExceeded);
                }
                let a = evm.stack.pop();
                let b = evm.stack.pop();

                if a.is_none() || b.is_none() {
                    return ExecutionStepOutcome::Revert(RevertReason::NotEnoughValuesOnStack);
                }

                if a.unwrap() > b.unwrap() {
                    evm.stack.push(U256::from(1));
                } else {
                    evm.stack.push(U256::from(0));
                }
                ExecutionStepOutcome::Ok
            }
            Opcode::SLT => {
                if Opcode::exceeded_prepaid_gas(evm, 3) {
                    return ExecutionStepOutcome::Revert(RevertReason::GasExceeded);
                }

                let a = evm.stack.pop();
                let b = evm.stack.pop();

                if a.is_none() || b.is_none() {
                    return ExecutionStepOutcome::Revert(RevertReason::NotEnoughValuesOnStack);
                }

                if I256::from(a.unwrap()) < I256::from(b.unwrap()) {
                    evm.stack.push(U256::from(1));
                } else {
                    evm.stack.push(U256::from(0));
                }

                ExecutionStepOutcome::Ok
            }
            Opcode::SGT => {
                if Opcode::exceeded_prepaid_gas(evm, 3) {
                    return ExecutionStepOutcome::Revert(RevertReason::GasExceeded);
                }

                let a = evm.stack.pop();
                let b = evm.stack.pop();

                if a.is_none() || b.is_none() {
                    return ExecutionStepOutcome::Revert(RevertReason::NotEnoughValuesOnStack);
                }

                if I256::from(a.unwrap()) > I256::from(b.unwrap()) {
                    evm.stack.push(U256::from(1));
                } else {
                    evm.stack.push(U256::from(0));
                }

                ExecutionStepOutcome::Ok
            }
            Opcode::EQ => {
                if Opcode::exceeded_prepaid_gas(evm, 3) {
                    return ExecutionStepOutcome::Revert(RevertReason::GasExceeded);
                }
                let a = evm.stack.pop();
                let b = evm.stack.pop();

                if a.is_none() || b.is_none() {
                    return ExecutionStepOutcome::Revert(RevertReason::NotEnoughValuesOnStack);
                }

                if a.unwrap() == b.unwrap() {
                    evm.stack.push(U256::from(1));
                } else {
                    evm.stack.push(U256::from(0));
                }
                ExecutionStepOutcome::Ok
            }
            Opcode::ISZERO => {
                if Opcode::exceeded_prepaid_gas(evm, 3) {
                    return ExecutionStepOutcome::Revert(RevertReason::GasExceeded);
                }
                let a = evm.stack.pop();

                if a.is_none() {
                    return ExecutionStepOutcome::Revert(RevertReason::NotEnoughValuesOnStack);
                }

                if a.unwrap() == U256::from(0) {
                    evm.stack.push(U256::from(1));
                } else {
                    evm.stack.push(U256::from(0));
                }
                ExecutionStepOutcome::Ok
            }
            Opcode::AND => {
                if Opcode::exceeded_prepaid_gas(evm, 3) {
                    return ExecutionStepOutcome::Revert(RevertReason::GasExceeded);
                }

                let a = evm.stack.pop();
                let b = evm.stack.pop();

                if a.is_none() || b.is_none() {
                    return ExecutionStepOutcome::Revert(RevertReason::NotEnoughValuesOnStack);
                }

                let result = a.unwrap() & b.unwrap();
                evm.stack.push(result);

                ExecutionStepOutcome::Ok
            }
            Opcode::OR => {
                if Opcode::exceeded_prepaid_gas(evm, 3) {
                    return ExecutionStepOutcome::Revert(RevertReason::GasExceeded);
                }

                let a = evm.stack.pop();
                let b = evm.stack.pop();

                if a.is_none() || b.is_none() {
                    return ExecutionStepOutcome::Revert(RevertReason::NotEnoughValuesOnStack);
                }

                let result = a.unwrap() | b.unwrap();
                evm.stack.push(result);

                ExecutionStepOutcome::Ok
            }
            Opcode::XOR => {
                if Opcode::exceeded_prepaid_gas(evm, 3) {
                    return ExecutionStepOutcome::Revert(RevertReason::GasExceeded);
                }

                let a = evm.stack.pop();
                let b = evm.stack.pop();

                if a.is_none() || b.is_none() {
                    return ExecutionStepOutcome::Revert(RevertReason::NotEnoughValuesOnStack);
                }

                let result = a.unwrap() ^ b.unwrap();
                evm.stack.push(result);

                ExecutionStepOutcome::Ok
            }
            Opcode::NOT => {
                if Opcode::exceeded_prepaid_gas(evm, 3) {
                    return ExecutionStepOutcome::Revert(RevertReason::GasExceeded);
                }

                let a = evm.stack.pop();

                if a.is_none() {
                    return ExecutionStepOutcome::Revert(RevertReason::NotEnoughValuesOnStack);
                }

                let result = !a.unwrap();
                evm.stack.push(result);

                ExecutionStepOutcome::Ok
            }
            Opcode::BYTE => {
                if Opcode::exceeded_prepaid_gas(evm, 3) {
                    return ExecutionStepOutcome::Revert(RevertReason::GasExceeded);
                }

                let offset = evm.stack.pop();
                let x = evm.stack.pop();

                if offset.is_none() || x.is_none() {
                    return ExecutionStepOutcome::Revert(RevertReason::NotEnoughValuesOnStack);
                }

                let offset = offset.unwrap().as_usize();
                let x = x.unwrap();
                if offset > 31 {
                    evm.stack.push(U256::from(0));
                } else {
                    let byte = x.byte(31 - offset); // 31 - offset because of endianess
                    evm.stack.push(U256::from(byte));
                }

                ExecutionStepOutcome::Ok
            }
            Opcode::SHL => {
                if Opcode::exceeded_prepaid_gas(evm, 3) {
                    return ExecutionStepOutcome::Revert(RevertReason::GasExceeded);
                }

                let shift = evm.stack.pop();
                let value = evm.stack.pop();
                if shift.is_none() || value.is_none() {
                    return ExecutionStepOutcome::Revert(RevertReason::NotEnoughValuesOnStack);
                }

                let shift = shift.unwrap();
                let value = value.unwrap();

                if shift > U256::from(255) {
                    evm.stack.push(U256::from(0));
                } else {
                    let shifted_value = value << shift;
                    evm.stack.push(shifted_value);
                }

                ExecutionStepOutcome::Ok
            }
            Opcode::SHR => {
                if Opcode::exceeded_prepaid_gas(evm, 3) {
                    return ExecutionStepOutcome::Revert(RevertReason::GasExceeded);
                }

                let shift = evm.stack.pop();
                let value = evm.stack.pop();
                if shift.is_none() || value.is_none() {
                    return ExecutionStepOutcome::Revert(RevertReason::NotEnoughValuesOnStack);
                }

                let shift = shift.unwrap();
                let value = value.unwrap();

                if shift > U256::from(255) {
                    evm.stack.push(U256::from(0));
                } else {
                    let shifted_value = value >> shift;
                    evm.stack.push(shifted_value);
                }

                ExecutionStepOutcome::Ok
            }
            Opcode::SAR => {
                if Opcode::exceeded_prepaid_gas(evm, 3) {
                    return ExecutionStepOutcome::Revert(RevertReason::GasExceeded);
                }

                let shift = evm.stack.pop();
                let value = evm.stack.pop();
                if shift.is_none() || value.is_none() {
                    return ExecutionStepOutcome::Revert(RevertReason::NotEnoughValuesOnStack);
                }

                let shift = shift.unwrap();
                let value = I256::from(value.unwrap());

                if shift > U256::from(255) || value == I256::zero() {
                    let I256(sign, _) = value;
                    let to_push = match sign {
                        Sign::Plus | Sign::Zero => U256::from(0),
                        Sign::Minus => I256(Sign::Minus, U256::one()).into(),
                    };
                    evm.stack.push(to_push);
                } else {
                    let to_push = match value.0 {
                        Sign::Plus | Sign::Zero => value.1 >> shift,
                        Sign::Minus => {
                            let shifted = (value.1.overflowing_sub(U256::one()).0 >> shift)
                                .overflowing_add(U256::one())
                                .0;
                            I256(Sign::Minus, shifted).into()
                        }
                    };
                    // let shifted_value = value >> shift;
                    evm.stack.push(to_push);
                }

                ExecutionStepOutcome::Ok
            }
            Opcode::SHA3 => {
                let offset = evm.stack.pop();
                let size = evm.stack.pop();

                if offset.is_none() || size.is_none() {
                    return ExecutionStepOutcome::Revert(RevertReason::NotEnoughValuesOnStack);
                }
                let size = size.unwrap();
                let offset = offset.unwrap();

                // gas
                let minimum_word_size = (size + 31) / 32;
                let memory_expansion_cost = if offset.as_usize() <= evm.last_accessed_memory_offset
                {
                    0
                } else {
                    let memory_size_word = ((size + 31) / 32).as_usize();
                    let memory_cost = (memory_size_word.pow(2)) / 512 + (memory_size_word * 3);

                    let new_memory_cost = memory_cost - evm.last_memory_cost;

                    evm.last_memory_cost = new_memory_cost;
                    evm.last_accessed_memory_offset = offset.as_usize();
                    new_memory_cost
                };
                let gas =
                    U256::from(30) + U256::from(6) * minimum_word_size + memory_expansion_cost;
                if Opcode::exceeded_prepaid_gas(evm, gas.as_u128()) {
                    return ExecutionStepOutcome::Revert(RevertReason::GasExceeded);
                }

                evm.allocate_more_memory(offset.as_usize(), size.as_usize());
                let memory_to_hash = evm.get_memory_part(offset.as_usize(), size.as_usize());

                let mut hasher = Keccak256::new();
                hasher.update(memory_to_hash);
                let result = hasher.finalize();

                let value_to_add_on_stack = U256::from_str(&format!("{:x}", result)).unwrap();
                evm.stack.push(value_to_add_on_stack);

                ExecutionStepOutcome::Ok
            }
            Opcode::ADDRESS => {
                if Opcode::exceeded_prepaid_gas(evm, 2) {
                    return ExecutionStepOutcome::Revert(RevertReason::GasExceeded);
                }

                let addr = evm.context.get_address();
                evm.stack.push(U256::from(&addr[..]));
                ExecutionStepOutcome::Ok
            }
            Opcode::BALANCE => {
                if Opcode::exceeded_prepaid_gas(evm, 100) {
                    return ExecutionStepOutcome::Revert(RevertReason::GasExceeded);
                }

                let address = evm.stack.pop();

                if address.is_none() {
                    return ExecutionStepOutcome::Revert(RevertReason::NotEnoughValuesOnStack);
                }

                let balance = evm.context.get_balance_of(address.unwrap());

                if let Some(bal) = balance {
                    evm.stack.push(*bal);
                } else {
                    evm.stack.push(U256::from(0));
                }

                ExecutionStepOutcome::Ok
            }
            Opcode::ORIGIN => {
                if Opcode::exceeded_prepaid_gas(evm, 2) {
                    return ExecutionStepOutcome::Revert(RevertReason::GasExceeded);
                }

                let origin = evm.context.get_tx_origin();
                evm.stack.push(U256::from(&origin[..]));
                ExecutionStepOutcome::Ok
            }
            Opcode::CALLER => {
                if Opcode::exceeded_prepaid_gas(evm, 2) {
                    return ExecutionStepOutcome::Revert(RevertReason::GasExceeded);
                }

                let caller = evm.context.get_msg_sender();
                evm.stack.push(U256::from(&caller[..]));
                ExecutionStepOutcome::Ok
            }
            Opcode::CALLVALUE => {
                if Opcode::exceeded_prepaid_gas(evm, 2) {
                    return ExecutionStepOutcome::Revert(RevertReason::GasExceeded);
                }

                let call_value = evm.context.get_call_value();
                evm.stack.push(U256::from(call_value));
                ExecutionStepOutcome::Ok
            }
            Opcode::CALLDATALOAD => {
                if Opcode::exceeded_prepaid_gas(evm, 3) {
                    return ExecutionStepOutcome::Revert(RevertReason::GasExceeded);
                }

                let offset = evm.stack.pop();
                if offset.is_none() {
                    return ExecutionStepOutcome::Revert(RevertReason::NotEnoughValuesOnStack);
                }

                let offset = offset.unwrap().as_usize();
                let calldata_len = evm.context.calldata.len();

                let mut data = Vec::<u8>::with_capacity(32);
                let mut index = offset;
                while index < offset + 32 {
                    if index < calldata_len {
                        data.push(evm.context.calldata[index]);
                    } else {
                        data.push(0u8);
                    }

                    index += 1;
                }

                evm.stack.push(U256::from(&data[..]));

                ExecutionStepOutcome::Ok
            }
            Opcode::CALLDATASIZE => {
                if Opcode::exceeded_prepaid_gas(evm, 2) {
                    return ExecutionStepOutcome::Revert(RevertReason::GasExceeded);
                }

                let calldata_size = evm.context.calldata.len();
                evm.stack.push(U256::from(calldata_size));

                ExecutionStepOutcome::Ok
            }
            Opcode::CALLDATACOPY => {
                let dest_offset = evm.stack.pop();
                let offset = evm.stack.pop();
                let size = evm.stack.pop();

                if dest_offset.is_none() || offset.is_none() || size.is_none() {
                    return ExecutionStepOutcome::Revert(RevertReason::NotEnoughValuesOnStack);
                }

                let dest_offset = dest_offset.unwrap();
                let offset = offset.unwrap();
                let size = size.unwrap();

                // gas
                let minimum_word_size = (size + 31) / 32;
                let memory_expansion_cost =
                    if dest_offset.as_usize() <= evm.last_accessed_memory_offset {
                        0
                    } else {
                        let memory_size_word = ((size + 31) / 32).as_usize();
                        let memory_cost = (memory_size_word.pow(2)) / 512 + (memory_size_word * 3);

                        let new_memory_cost = memory_cost - evm.last_memory_cost;

                        evm.last_memory_cost = new_memory_cost;
                        evm.last_accessed_memory_offset = dest_offset.as_usize();
                        new_memory_cost
                    };
                let gas = U256::from(3) + U256::from(3) * minimum_word_size + memory_expansion_cost;
                if Opcode::exceeded_prepaid_gas(evm, gas.as_u128()) {
                    return ExecutionStepOutcome::Revert(RevertReason::GasExceeded);
                }

                evm.allocate_more_memory(dest_offset.as_usize(), size.as_usize());

                let mut calldata_index = offset.as_usize();
                let mut memory_index = dest_offset.as_usize();
                while calldata_index < offset.as_usize() + size.as_usize() {
                    let byte_to_copy = if calldata_index < evm.context.calldata.len() {
                        evm.context.calldata[calldata_index]
                    } else {
                        0u8
                    };

                    evm.memory[memory_index] = byte_to_copy;
                    calldata_index += 1;
                    memory_index += 1;
                }

                ExecutionStepOutcome::Ok
            }
            Opcode::CODESIZE => {
                if Opcode::exceeded_prepaid_gas(evm, 2) {
                    return ExecutionStepOutcome::Revert(RevertReason::GasExceeded);
                }

                evm.stack.push(U256::from(evm.code.len()));
                ExecutionStepOutcome::Ok
            }
            Opcode::CODECOPY => {
                let dest_offset = evm.stack.pop();
                let offset = evm.stack.pop();
                let size = evm.stack.pop();

                if dest_offset.is_none() || offset.is_none() || size.is_none() {
                    return ExecutionStepOutcome::Revert(RevertReason::NotEnoughValuesOnStack);
                }

                let dest_offset = dest_offset.unwrap();
                let offset = offset.unwrap();
                let size = size.unwrap();

                // gas
                let minimum_word_size = (size + 31) / 32;
                let memory_expansion_cost =
                    if dest_offset.as_usize() <= evm.last_accessed_memory_offset {
                        0
                    } else {
                        let memory_size_word = ((size + 31) / 32).as_usize();
                        let memory_cost = (memory_size_word.pow(2)) / 512 + (memory_size_word * 3);

                        let new_memory_cost = memory_cost - evm.last_memory_cost;

                        evm.last_memory_cost = new_memory_cost;
                        evm.last_accessed_memory_offset = dest_offset.as_usize();
                        new_memory_cost
                    };
                let gas = U256::from(3) + U256::from(3) * minimum_word_size + memory_expansion_cost;
                if Opcode::exceeded_prepaid_gas(evm, gas.as_u128()) {
                    return ExecutionStepOutcome::Revert(RevertReason::GasExceeded);
                }

                evm.allocate_more_memory(dest_offset.as_usize(), size.as_usize());

                let mut code_index = offset.as_usize();
                let mut memory_index = dest_offset.as_usize();
                while code_index < offset.as_usize() + size.as_usize() {
                    let byte_to_copy = if code_index < evm.code.len() {
                        evm.code_bytes[code_index]
                    } else {
                        0u8
                    };

                    evm.memory[memory_index] = byte_to_copy;
                    code_index += 1;
                    memory_index += 1;
                }

                ExecutionStepOutcome::Ok
            }
            Opcode::GASPRICE => {
                if Opcode::exceeded_prepaid_gas(evm, 2) {
                    return ExecutionStepOutcome::Revert(RevertReason::GasExceeded);
                }

                evm.stack.push(U256::from(evm.gas_price));
                ExecutionStepOutcome::Ok
            }
            Opcode::EXTCODESIZE => {
                if Opcode::exceeded_prepaid_gas(evm, 100) {
                    return ExecutionStepOutcome::Revert(RevertReason::GasExceeded);
                }

                let address = evm.stack.pop();
                if address.is_none() {
                    return ExecutionStepOutcome::Revert(RevertReason::NotEnoughValuesOnStack);
                }
                let address = format!("{:x}", address.unwrap());
                if let Some(code) = evm.other_contracts_code.get(&address) {
                    evm.stack.push(U256::from(code.len()));
                } else {
                    evm.stack.push(U256::zero());
                }

                ExecutionStepOutcome::Ok
            }
            Opcode::EXTCODECOPY => {
                let address = evm.stack.pop();
                let dest_offset = evm.stack.pop();
                let offset = evm.stack.pop();
                let size = evm.stack.pop();

                if address.is_none() || dest_offset.is_none() || offset.is_none() || size.is_none()
                {
                    return ExecutionStepOutcome::Revert(RevertReason::NotEnoughValuesOnStack);
                }

                let address = address.unwrap();
                let dest_offset = dest_offset.unwrap();
                let offset = offset.unwrap();
                let size = size.unwrap();

                // gas
                let minimum_word_size = (size + 31) / 32;
                let memory_expansion_cost =
                    if dest_offset.as_usize() <= evm.last_accessed_memory_offset {
                        0
                    } else {
                        let memory_size_word = ((size + 31) / 32).as_usize();
                        let memory_cost = (memory_size_word.pow(2)) / 512 + (memory_size_word * 3);

                        let new_memory_cost = memory_cost - evm.last_memory_cost;

                        evm.last_memory_cost = new_memory_cost;
                        evm.last_accessed_memory_offset = dest_offset.as_usize();
                        new_memory_cost
                    };
                let gas = U256::from(3) * minimum_word_size + memory_expansion_cost + 100;
                if Opcode::exceeded_prepaid_gas(evm, gas.as_u128()) {
                    return ExecutionStepOutcome::Revert(RevertReason::GasExceeded);
                }
                let address_str = format!("{:x}", address);
                let code = if let Some(c) = evm.other_contracts_code.get(&address_str) {
                    c.clone()
                } else {
                    Vec::new()
                };

                evm.allocate_more_memory(dest_offset.as_usize(), size.as_usize());

                let mut code_index = offset.as_usize();
                let mut memory_index = dest_offset.as_usize();
                while code_index < offset.as_usize() + size.as_usize() {
                    let byte_to_copy = if code_index < code.len() {
                        code[code_index]
                    } else {
                        0u8
                    };

                    evm.memory[memory_index] = byte_to_copy;
                    code_index += 1;
                    memory_index += 1;
                }

                ExecutionStepOutcome::Ok
            }
            Opcode::RETURNDATASIZE => todo!(),
            Opcode::RETURNDATACOPY => todo!(),
            Opcode::EXTCODEHASH => todo!(),
            Opcode::BLOCKHASH => {
                if Opcode::exceeded_prepaid_gas(evm, 20) {
                    return ExecutionStepOutcome::Revert(RevertReason::GasExceeded);
                }

                let block_number = evm.stack.pop();
                if block_number.is_none() {
                    return ExecutionStepOutcome::Revert(RevertReason::NotEnoughValuesOnStack);
                }

                let block_number = block_number.unwrap();
                if block_number.as_u128() < evm.current_block_number - 256
                    || block_number.as_u128() > evm.current_block_number
                {
                    evm.stack.push(U256::zero());
                } else {
                    let hash = evm.get_block_hash_of(block_number.as_u128()).unwrap();
                    let hash_bytes: Vec<u8> = (0..hash.len())
                        .step_by(2)
                        .map(|i| u8::from_str_radix(&hash[i..i + 2], 16).unwrap())
                        .collect();
                    evm.stack.push(U256::from(&hash_bytes[..]));
                }

                ExecutionStepOutcome::Ok
            }
            Opcode::COINBASE => {
                if Opcode::exceeded_prepaid_gas(evm, 2) {
                    return ExecutionStepOutcome::Revert(RevertReason::GasExceeded);
                }

                let miner = evm.get_current_block_miner();
                let miner_bytes: Vec<u8> = (0..miner.len())
                    .step_by(2)
                    .map(|i| u8::from_str_radix(&miner[i..i + 2], 16).unwrap())
                    .collect();

                evm.stack.push(U256::from(&miner_bytes[..]));

                ExecutionStepOutcome::Ok
            }
            Opcode::TIMESTAMP => {
                if Opcode::exceeded_prepaid_gas(evm, 2) {
                    return ExecutionStepOutcome::Revert(RevertReason::GasExceeded);
                }

                evm.stack.push(U256::from(evm.get_timestamp()));
                ExecutionStepOutcome::Ok
            }
            Opcode::NUMBER => {
                if Opcode::exceeded_prepaid_gas(evm, 2) {
                    return ExecutionStepOutcome::Revert(RevertReason::GasExceeded);
                }

                evm.stack.push(U256::from(evm.get_block_number()));
                ExecutionStepOutcome::Ok
            }
            Opcode::DIFFICULTY => {
                if Opcode::exceeded_prepaid_gas(evm, 2) {
                    return ExecutionStepOutcome::Revert(RevertReason::GasExceeded);
                }

                evm.stack.push(U256::from(evm.get_current_difficulty()));
                ExecutionStepOutcome::Ok
            }
            Opcode::GASLIMIT => {
                if Opcode::exceeded_prepaid_gas(evm, 2) {
                    return ExecutionStepOutcome::Revert(RevertReason::GasExceeded);
                }

                evm.stack.push(U256::from(evm.get_gas_limit()));
                ExecutionStepOutcome::Ok
            }
            Opcode::CHAINID => {
                if Opcode::exceeded_prepaid_gas(evm, 2) {
                    return ExecutionStepOutcome::Revert(RevertReason::GasExceeded);
                }

                evm.stack.push(U256::from(evm.get_chain_id()));
                ExecutionStepOutcome::Ok
            }
            Opcode::SELFBALANCE => todo!(),
            Opcode::BASEFEE => todo!(),
            Opcode::POP => todo!(),
            Opcode::MLOAD => todo!(),
            Opcode::MSTORE => todo!(),
            Opcode::MSTORE8 => todo!(),
            Opcode::SLOAD => todo!(),
            Opcode::SSTORE => todo!(),
            Opcode::JUMP => todo!(),
            Opcode::JUMPI => todo!(),
            Opcode::PC => todo!(),
            Opcode::MSIZE => todo!(),
            Opcode::GAS => todo!(),
            Opcode::JUMPDEST => todo!(),
            Opcode::PUSH1(_) => todo!(),
            Opcode::PUSH2(_) => todo!(),
            Opcode::PUSH3(_) => todo!(),
            Opcode::PUSH4(_) => todo!(),
            Opcode::PUSH5(_) => todo!(),
            Opcode::PUSH6(_) => todo!(),
            Opcode::PUSH7(_) => todo!(),
            Opcode::PUSH8(_) => todo!(),
            Opcode::PUSH9(_) => todo!(),
            Opcode::PUSH10(_) => todo!(),
            Opcode::PUSH11(_) => todo!(),
            Opcode::PUSH12(_) => todo!(),
            Opcode::PUSH13(_) => todo!(),
            Opcode::PUSH14(_) => todo!(),
            Opcode::PUSH15(_) => todo!(),
            Opcode::PUSH16(_) => todo!(),
            Opcode::PUSH17(_) => todo!(),
            Opcode::PUSH18(_) => todo!(),
            Opcode::PUSH19(_) => todo!(),
            Opcode::PUSH20(_) => todo!(),
            Opcode::PUSH21(_) => todo!(),
            Opcode::PUSH22(_) => todo!(),
            Opcode::PUSH23(_) => todo!(),
            Opcode::PUSH24(_) => todo!(),
            Opcode::PUSH25(_) => todo!(),
            Opcode::PUSH26(_) => todo!(),
            Opcode::PUSH27(_) => todo!(),
            Opcode::PUSH28(_) => todo!(),
            Opcode::PUSH29(_) => todo!(),
            Opcode::PUSH30(_) => todo!(),
            Opcode::PUSH31(_) => todo!(),
            Opcode::PUSH32(_) => todo!(),
            Opcode::DUP1 => todo!(),
            Opcode::DUP2 => todo!(),
            Opcode::DUP3 => todo!(),
            Opcode::DUP4 => todo!(),
            Opcode::DUP5 => todo!(),
            Opcode::DUP6 => todo!(),
            Opcode::DUP7 => todo!(),
            Opcode::DUP8 => todo!(),
            Opcode::DUP9 => todo!(),
            Opcode::DUP10 => todo!(),
            Opcode::DUP11 => todo!(),
            Opcode::DUP12 => todo!(),
            Opcode::DUP13 => todo!(),
            Opcode::DUP14 => todo!(),
            Opcode::DUP15 => todo!(),
            Opcode::DUP16 => todo!(),
            Opcode::SWAP1 => todo!(),
            Opcode::SWAP2 => todo!(),
            Opcode::SWAP3 => todo!(),
            Opcode::SWAP4 => todo!(),
            Opcode::SWAP5 => todo!(),
            Opcode::SWAP6 => todo!(),
            Opcode::SWAP7 => todo!(),
            Opcode::SWAP8 => todo!(),
            Opcode::SWAP9 => todo!(),
            Opcode::SWAP10 => todo!(),
            Opcode::SWAP11 => todo!(),
            Opcode::SWAP12 => todo!(),
            Opcode::SWAP13 => todo!(),
            Opcode::SWAP14 => todo!(),
            Opcode::SWAP15 => todo!(),
            Opcode::SWAP16 => todo!(),
            Opcode::LOG0 => todo!(),
            Opcode::LOG1 => todo!(),
            Opcode::LOG2 => todo!(),
            Opcode::LOG3 => todo!(),
            Opcode::LOG4 => todo!(),
            Opcode::CREATE => todo!(),
            Opcode::CALL => todo!(),
            Opcode::CALLCODE => todo!(),
            Opcode::RETURN => todo!(),
            Opcode::DELEGATECALL => todo!(),
            Opcode::CREATE2 => todo!(),
            Opcode::STATICCALL => todo!(),
            Opcode::REVERT => todo!(),
            Opcode::SELFDESTRUCT => todo!(),
            Opcode::INVALID => panic!("Undefined opcode"),
        }
    }
}
