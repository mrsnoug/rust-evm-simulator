#[cfg(test)]
mod tests {
    use crate::evm_types::*;
    use primitive_types::U256;

    #[test]
    fn parsing_bytecode_works() {
        let bytecode = "600b6100150160005360106000f3".to_owned();
        let evm = Evm::parse_bytecode(bytecode);

        let code = evm.code;

        assert_eq!(code.len(), 8);
        assert_eq!(code[0], Opcode::PUSH1(vec![0x0b]));
        assert_eq!(code[1], Opcode::PUSH2(vec![0x00, 0x15]));
        assert_eq!(code[2], Opcode::ADD);
        assert_eq!(code[3], Opcode::PUSH1(vec![0x00]));
        assert_eq!(code[4], Opcode::MSTORE8);
        assert_eq!(code[5], Opcode::PUSH1(vec![0x10]));
        assert_eq!(code[6], Opcode::PUSH1(vec![0x00]));
        assert_eq!(code[7], Opcode::RETURN);
    }

    #[test]
    fn not_defined_opcode_results_in_invalid_code() {
        let bytecode = "600b6100150160005360106000f6f3".to_owned();
        let evm = Evm::parse_bytecode(bytecode);

        assert!(!evm.is_code_valid());
    }

    #[test]
    fn test_evm_revert() {
        let mut evm = Evm::new();
        let stack = vec![U256::from(10)];
        evm.add_opcode(Opcode::ADD); // requires 2 items on stack
        evm.set_gas(100000);
        evm.set_stack(stack);

        let step_result = evm.execute_step();
        assert_eq!(
            step_result,
            ExecutionStepOutcome::Revert(RevertReason::NotEnoughValuesOnStack)
        );

        evm.set_gas(1);
        evm.add_opcode(Opcode::BALANCE); // requires 100 gas
        let addr = "9bbfed6889322e016e0a02ee459d306fc19545d8".to_owned();
        let addr_bytes: Vec<u8> = (0..addr.len())
            .step_by(2)
            .map(|i| u8::from_str_radix(&addr[i..i + 2], 16).unwrap())
            .collect();
        let stack = vec![U256::from(&addr_bytes[..])];
        evm.set_stack(stack);

        let step_result = evm.execute_step();
        assert_eq!(
            step_result,
            ExecutionStepOutcome::Revert(RevertReason::GasExceeded)
        );
    }

    fn generic_testcase(stack: Vec<U256>, expected_result: U256, opcode: Opcode) {
        let mut evm = Evm::new();
        evm.set_stack(stack);
        evm.add_opcode(opcode);
        evm.set_gas(1000000);

        let step_result = evm.execute_step();
        assert_eq!(step_result, ExecutionStepOutcome::Ok);

        let result = evm.get_stack().pop().unwrap();
        assert_eq!(result, expected_result);
    }

    #[test]
    fn test_mul() {
        let stack = vec![
            U256::from_big_endian(
                &hex::decode("FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF")
                    .unwrap(),
            ),
            U256::from_big_endian(
                &hex::decode("FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF")
                    .unwrap(),
            ),
        ];
        generic_testcase(stack, U256::from(1), Opcode::MUL);
    }

    #[test]
    fn test_mod() {
        let stack = vec![U256::from(3), U256::from(10)];
        generic_testcase(stack, U256::from(1), Opcode::MOD);

        let stack = vec![U256::from(5), U256::from(17)];
        generic_testcase(stack, U256::from(2), Opcode::MOD);
    }

    #[test]
    fn test_smod() {
        let stack = vec![U256::from(3), U256::from(10)];
        generic_testcase(stack, U256::from(1), Opcode::SMOD);

        let stack = vec![
            U256::from_big_endian(
                &hex::decode("FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD")
                    .unwrap(),
            ),
            U256::from_big_endian(
                &hex::decode("FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8")
                    .unwrap(),
            ),
        ];
        generic_testcase(
            stack,
            U256::from_big_endian(
                &hex::decode("FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE")
                    .unwrap(),
            ),
            Opcode::SMOD,
        );
    }

    #[test]
    fn test_addmod() {
        let tmp = U256::from_big_endian(
            &hex::decode("FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF")
                .unwrap(),
        );

        let stack = vec![U256::from(2), U256::from(2), tmp];
        generic_testcase(stack, U256::from(1), Opcode::ADDMOD);

        let stack = vec![U256::from(8), U256::from(10), U256::from(10)];
        generic_testcase(stack, U256::from(4), Opcode::ADDMOD);
    }

    #[test]
    fn test_mulmod() {
        let stack = vec![U256::from(8), U256::from(10), U256::from(10)];
        generic_testcase(stack, U256::from(4), Opcode::MULMOD);

        let stack = vec![
            U256::from(12),
            U256::from_big_endian(
                &hex::decode("FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF")
                    .unwrap(),
            ),
            U256::from_big_endian(
                &hex::decode("FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF")
                    .unwrap(),
            ),
        ];
        generic_testcase(stack, U256::from(9), Opcode::MULMOD);
    }

    fn exp_testcase(stack: Vec<U256>, expected_result: U256, expected_gas_burned: u128) {
        let mut evm = Evm::new();
        evm.set_stack(stack);
        evm.add_opcode(Opcode::EXP);

        let gas = 1000000;
        evm.set_gas(gas);

        let step_result = evm.execute_step();
        assert_eq!(step_result, ExecutionStepOutcome::Ok);

        let result = evm.get_stack().pop().unwrap();
        assert_eq!(result, expected_result);

        let remaining_gas = evm.get_gas();
        assert_eq!(remaining_gas, gas - expected_gas_burned);
    }

    #[test]
    fn test_exp() {
        let stack1 = vec![U256::from(2), U256::from(10)];
        exp_testcase(stack1, U256::from(100), 60);

        let stack2 = vec![U256::from(2), U256::from(2)];
        exp_testcase(stack2, U256::from(4), 60);

        let stack2 = vec![U256::from(312), U256::from(3)];
        exp_testcase(
            stack2,
            U256::from_big_endian(
                &hex::decode("b349a52a9e4dfc7afa92293aa1de668e877992fec40c94c4ae50a6e55e561b61")
                    .unwrap(),
            ),
            110,
        );
    }

    #[test]
    fn test_comparisons() {
        let stack1 = vec![U256::from(10), U256::from(9)];
        generic_testcase(stack1, U256::from(1), Opcode::LT);

        let stack2 = vec![U256::from(10), U256::from(10)];
        generic_testcase(stack2, U256::from(0), Opcode::LT);

        let stack3 = vec![U256::from(9), U256::from(10)];
        generic_testcase(stack3, U256::from(1), Opcode::GT);

        let stack4 = vec![U256::from(10), U256::from(10)];
        generic_testcase(stack4, U256::from(0), Opcode::GT);

        let stack5 = vec![
            U256::from(0),
            U256::from_big_endian(
                &hex::decode("FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF")
                    .unwrap(),
            ),
        ];
        generic_testcase(stack5, U256::from(1), Opcode::SLT);

        let stack6 = vec![U256::from(10), U256::from(10)];
        generic_testcase(stack6, U256::from(0), Opcode::SLT);

        let stack7 = vec![
            U256::from_big_endian(
                &hex::decode("FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF")
                    .unwrap(),
            ),
            U256::from(0),
        ];
        generic_testcase(stack7, U256::from(1), Opcode::SGT);

        let stack8 = vec![U256::from(10), U256::from(10)];
        generic_testcase(stack8, U256::from(0), Opcode::SGT);

        let stack9 = vec![U256::from(10), U256::from(10)];
        generic_testcase(stack9, U256::from(1), Opcode::EQ);

        let stack10 = vec![U256::from(5), U256::from(10)];
        generic_testcase(stack10, U256::from(0), Opcode::EQ);

        let stack11 = vec![U256::from(10)];
        generic_testcase(stack11, U256::from(0), Opcode::ISZERO);

        let stack12 = vec![U256::from(0)];
        generic_testcase(stack12, U256::from(1), Opcode::ISZERO);
    }

    #[test]
    fn test_logical() {
        let stack = vec![U256::from(0xF), U256::from(0xF)];
        generic_testcase(stack, U256::from(0xF), Opcode::AND);

        let stack = vec![U256::from(0), U256::from(0xFF)];
        generic_testcase(stack, U256::from(0), Opcode::AND);

        let stack = vec![U256::from(0xF), U256::from(0xF0)];
        generic_testcase(stack, U256::from(0xFF), Opcode::OR);

        let stack = vec![U256::from(0xFF), U256::from(0xFF)];
        generic_testcase(stack, U256::from(0xFF), Opcode::OR);

        let stack = vec![U256::from(0xF), U256::from(0xF0)];
        generic_testcase(stack, U256::from(0xFF), Opcode::XOR);

        let stack = vec![U256::from(0xFF), U256::from(0xFF)];
        generic_testcase(stack, U256::from(0), Opcode::XOR);

        let stack = vec![U256::from(0)];
        generic_testcase(
            stack,
            U256::from_big_endian(
                &hex::decode("FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF")
                    .unwrap(),
            ),
            Opcode::NOT,
        );

        let stack = vec![U256::from(1), U256::from(1)];
        generic_testcase(stack, U256::from(2), Opcode::SHL);

        let stack = vec![
            U256::from_big_endian(
                &hex::decode("FF00000000000000000000000000000000000000000000000000000000000000")
                    .unwrap(),
            ),
            U256::from(4),
        ];
        generic_testcase(
            stack,
            U256::from_big_endian(
                &hex::decode("F000000000000000000000000000000000000000000000000000000000000000")
                    .unwrap(),
            ),
            Opcode::SHL,
        );

        let stack = vec![U256::from(2), U256::from(1)];
        generic_testcase(stack, U256::from(1), Opcode::SHR);

        let stack = vec![U256::from(0xFF), U256::from(4)];
        generic_testcase(stack, U256::from(0xF), Opcode::SHR);

        let stack = vec![U256::from(2), U256::from(1)];
        generic_testcase(stack, U256::from(1), Opcode::SAR);

        let stack = vec![
            U256::from_big_endian(
                &hex::decode("FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0")
                    .unwrap(),
            ),
            U256::from(4),
        ];
        generic_testcase(
            stack,
            U256::from_big_endian(
                &hex::decode("FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF")
                    .unwrap(),
            ),
            Opcode::SAR,
        );
    }

    #[test]
    fn test_address() {
        let mut evm = Evm::new();
        let mut context = Context::new();
        context.set_address_hex("9bbfed6889322e016e0a02ee459d306fc19545d8".to_owned());
        evm.set_context(context);
        evm.add_opcode(Opcode::ADDRESS);
        evm.set_gas(1000000);

        let step_result = evm.execute_step();
        assert_eq!(step_result, ExecutionStepOutcome::Ok);

        let result = evm.get_stack().pop().unwrap();
        let result_str = format!("{:x}", result);
        assert_eq!(
            result_str,
            "9bbfed6889322e016e0a02ee459d306fc19545d8".to_owned()
        );
    }

    #[test]
    fn test_balance() {
        let mut evm = Evm::new();
        let mut context = Context::new();
        let expected_balance = U256::from(100000);
        let addr = "9bbfed6889322e016e0a02ee459d306fc19545d8".to_owned();
        context.set_balance_of(addr.clone(), expected_balance);
        evm.set_context(context);
        evm.add_opcode(Opcode::BALANCE);
        evm.set_gas(100000);

        let addr_bytes: Vec<u8> = (0..addr.len())
            .step_by(2)
            .map(|i| u8::from_str_radix(&addr[i..i + 2], 16).unwrap())
            .collect();
        let stack = vec![U256::from(&addr_bytes[..])];
        evm.set_stack(stack);

        let step_result = evm.execute_step();
        assert_eq!(step_result, ExecutionStepOutcome::Ok);

        let result = evm.get_stack().pop().unwrap();
        assert_eq!(result, expected_balance);
    }

    #[test]
    fn test_sha3() {
        let mut evm = Evm::new();
        let memory = vec![0xFF, 0xFF, 0xFF, 0xFF];
        evm.set_memory(memory);

        let stack = vec![U256::from(4), U256::from(0)];
        evm.set_stack(stack);

        let gas = 100000;
        evm.set_gas(gas);
        evm.add_opcode(Opcode::SHA3);

        let step_result = evm.execute_step();
        assert_eq!(step_result, ExecutionStepOutcome::Ok);

        let result = evm.get_stack().pop().unwrap();
        let result_hex = format!("{:x}", result).to_uppercase();
        assert_eq!(
            result_hex,
            "29045A592007D0C246EF02C2223570DA9522D0CF0F73282C79A1BC8F0BB2C238"
        );

        assert_eq!(evm.gas, gas - 36);
    }

    #[test]
    fn test_byte() {
        let stack = vec![U256::from(0xFF), U256::from(31)];
        generic_testcase(stack, U256::from(0xFF), Opcode::BYTE);

        let stack = vec![U256::from(0xFF00), U256::from(30)];
        generic_testcase(stack, U256::from(0xFF), Opcode::BYTE);

        let stack = vec![U256::from(0xFF00), U256::from(36)];
        generic_testcase(stack, U256::from(0x0), Opcode::BYTE);
    }

    #[test]
    fn test_calldataload() {
        let mut evm = Evm::new();
        let mut context = Context::new();
        let mut calldata = Vec::new();
        for _ in 0..32 {
            calldata.push(0xFFu8);
        }
        context.set_calldata(calldata);
        evm.set_context(context);

        let stack = vec![U256::from(0)];
        evm.set_stack(stack);

        evm.set_gas(100000);
        evm.add_opcode(Opcode::CALLDATALOAD);

        let step_result = evm.execute_step();
        assert_eq!(step_result, ExecutionStepOutcome::Ok);

        let result = evm.stack.pop().unwrap();
        assert_eq!(
            result,
            U256::from_big_endian(
                &hex::decode("FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF")
                    .unwrap(),
            )
        );

        let stack = vec![U256::from(31)];
        evm.set_stack(stack);
        evm.add_opcode(Opcode::CALLDATALOAD);

        let step_result = evm.execute_step();
        assert_eq!(step_result, ExecutionStepOutcome::Ok);

        let result = evm.stack.pop().unwrap();
        assert_eq!(
            result,
            U256::from_big_endian(
                &hex::decode("FF00000000000000000000000000000000000000000000000000000000000000")
                    .unwrap(),
            )
        );

        evm.set_stack(vec![]);
        evm.add_opcode(Opcode::CALLDATASIZE);

        let step_result = evm.execute_step();
        assert_eq!(step_result, ExecutionStepOutcome::Ok);
        let result = evm.stack.pop().unwrap();
        assert_eq!(result, U256::from(32));

        evm.add_opcode(Opcode::CALLDATASIZE);
        evm.set_stack(vec![]);
        evm.context.set_calldata(vec![0xFF]);
        let step_result = evm.execute_step();
        assert_eq!(step_result, ExecutionStepOutcome::Ok);
        let result = evm.stack.pop().unwrap();
        assert_eq!(result, U256::from(1));
    }

    #[test]
    fn test_calldatacopy() {
        let mut evm = Evm::new();
        let mut context = Context::new();
        let mut calldata = Vec::with_capacity(32);
        for _ in 0..32 {
            calldata.push(0xFFu8);
        }
        context.set_calldata(calldata);
        evm.set_context(context);
        evm.set_gas(100000000);

        let stack = vec![U256::from(32), U256::from(0), U256::from(0)];
        evm.set_stack(stack);

        evm.add_opcode(Opcode::CALLDATACOPY);

        let step_result = evm.execute_step();
        assert_eq!(step_result, ExecutionStepOutcome::Ok);

        let mut expected_memory_result = Vec::with_capacity(32);
        for _ in 0..32 {
            expected_memory_result.push(0xFFu8);
        }
        assert_eq!(expected_memory_result, evm.memory);

        evm.add_opcode(Opcode::CALLDATACOPY);
        let stack = vec![U256::from(8), U256::from(31), U256::from(0)];
        evm.set_stack(stack);

        let step_result = evm.execute_step();
        assert_eq!(step_result, ExecutionStepOutcome::Ok);

        let mut expected_memory_result = vec![
            0xFFu8, 0x00u8, 0x00u8, 0x00u8, 0x00u8, 0x00u8, 0x00u8, 0x00u8,
        ];
        for _ in 0..24 {
            expected_memory_result.push(0xFF);
        }

        assert_eq!(expected_memory_result, evm.memory);
    }
}
