# Rust EVM simulator

This is a pet project to better understand EVM's internals. It is by no means meant to be used commercially.

It is still very early in development, will work on it whenever I have time.
A target, for now, is to implement a simulator that will execute EVM bytecode within a given (configurable) environment.
